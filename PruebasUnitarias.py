

import unittest
import ProyectoFinal

class TestProyectoFinal(unittest.TestCase):
    
    def test_fSuma(self):
        self.assertEqual(ProyectoFinal.fSuma(5, 5), 10)
        self.assertEqual(ProyectoFinal.fSuma(10, 15), 25)
        self.assertEqual(ProyectoFinal.fSuma(100, 350), 450)
        self.assertEqual(ProyectoFinal.fSuma(1000, 370), 1370)
        self.assertEqual(ProyectoFinal.fSuma(1.28, 1.28), 2.56)


    def test_fResta(self):
        self.assertEqual(ProyectoFinal.fResta(4, 2), 2)
        self.assertEqual(ProyectoFinal.fResta(14, 8), 6)
        self.assertEqual(ProyectoFinal.fResta(140, 80), 60)
        self.assertEqual(ProyectoFinal.fResta(1400, 492), 908)
        self.assertEqual(ProyectoFinal.fResta(4.8, 3.65), 1.15)


    def test_fMultiplicacion(self):
        self.assertEqual(ProyectoFinal.fMultiplicacion(2, 2), 4)
        self.assertEqual(ProyectoFinal.fMultiplicacion(12, 6), 72)
        self.assertEqual(ProyectoFinal.fMultiplicacion(120, 21), 2520)
        self.assertEqual(ProyectoFinal.fMultiplicacion(1200, 4), 4800)
        self.assertEqual(ProyectoFinal.fMultiplicacion(2.75, 1.3), 3.575)

    def test_fDivision(self):
        self.assertEqual(ProyectoFinal.fDivision(8, 2), 4)
        self.assertEqual(ProyectoFinal.fDivision(80, 4), 20)
        self.assertEqual(ProyectoFinal.fDivision(800, 10), 80)
        self.assertEqual(ProyectoFinal.fDivision(8000, 40), 200)
        self.assertEqual(ProyectoFinal.fDivision(8.7, 2.4), 3.625)





if __name__ == "__main__":
    unittest.main()
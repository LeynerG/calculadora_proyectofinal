.. Calculadora_ProyectoFinal documentation master file, created by
   sphinx-quickstart on Sun Apr 21 15:34:48 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentacion - Calculadora
=====================================================
.. image:: _static/calculadoraa.PNG
    :align: center
    :alt: Calculadora

Calculadora es una app, que permite realizar operaciones matematicas como *suma*, *resta*, *multiplicacion* y *division*.

Esta compuesta por las siguientes funciones:


.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: ProyectoFinal
   :members:
   :special-members:






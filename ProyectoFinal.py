# Leyner Garzon AP-50-619
# Rene De Leon 2-713-1724
# Roman Jaramillo 8-929-1986
# Abdiel Hernández 4-787-553
# Alvaro Sánchez 8-845-1862
# Alexis Hinestroza 8-796-852
# Lusinio Serrano 1-738-1162
# Moises Perez 8-905-660


import os #lo necesitamos para luego poder hacer una limpieza de pantalla con el cls


def menu():
	"""
	Esta funcion crea un menu en el cual se muestran las opciones a elegir
	"""
	os.system('cls')#para limpiar la pantalla 
	print("\n\t\t--- MENU ---\n")
	print("\t1. Suma")
	print("\t2. Resta")
	print("\t3. Multiplicacion")
	print("\t4. Division")	
	print("\t0. Salir")



def opcion(min, max, salida):
	"""
	Esta funcion muestra el menu, captura y valida la opcion que el usario ingrese, ademas recibe el numero mayor y menor, para validar la opcion, 
	si la opcion se encuentra fuera de rango mostrara un error.
	"""
	#:param min: Opcion minima.
	#:param max: Opcion maxima.
	#:param salida: Opcion para salir del programa
	#:type min: int
	#:type max: int 
	#:type salida: int
	#:return: Retorna la opcion escogida
	#:rtype: int 
	
	opcion= -1
	
	menu() #imprime el menu
	opcion= int(input("\n\tOpcion: ")) #permite al usuario ingresar una opcion 
	while (opcion < min) or (opcion > max): #lo que hace esto es que si la opcion es menor o mayor a los numeros que hay en el menu salta error
		print("\n\t>>> Error. Opcion invalida.")
		print("\t    Presione una tecla reiniciar el programa ...")
		input() #reinicia el programa cuando se ingresa cualquier tecla
		
		menu() 
		opcion= int(input("\n\tOpcion: ")) 
	
	return opcion
		
		


def submenu(titulo):
	"""
	Esta funcion muestra el encabezado de cada submenu, lo convierte a mayuscula y le pone guiones a los lados como decoracion.
	"""
	
	print("\n\t\t--- " + titulo.upper() + " ---\n") #lo que hace es convertir los titulos a mayuscula cerrada y agregarle los guiones sin necesidad de hacerlo en cada funcion 


def fSuma(num1,num2):
	"""
	Esta funcion calcula la suma de dos numeros en base a dos entradas de tipo float, num1 y num2, son de tipo float para poder realizar operaciones con decimales.
	"""
	#:param num1: Primero numero
	#:param num2: Segundo Numero
	#:type num1: float
	#:type num2: float
	#:return: Retorna la suma de num1+num2
	#:rtype: float
	
	
	#str(num) : es una funcion que convierte el numero num en texto para imprimirlo
	print("\n\t-> El resultado de " + str(num1) + " + " + str(num2) + "= ", num1+num2)
	print("\t   Presione cualquier tecla para reiniciar el programa ...")
	input()
	return num1+num2
	
	

def fResta(num1,num2):
	"""
	Esta funcion calcula la resta de dos numeros en base a dos entradas de tipo float, num1 y num2, son de tipo float para poder realizar operaciones con decimales.
	"""
	#:param num1: Primero numero
	#:param num2: Segundo Numero
	#:type num1: float
	#:type num2: float
	#:return: Retorna la resta de num1-num2
	#:rtype: float
	

	print("\n\t-> El resultado de " + str(num1) + " - " + str(num2) + "= ", num1-num2)
	print("\t   Presione cualquier tecla para reiniciar el programa ...")
	input()
	return num1-num2
	
	
#funcion que calcula la multiplicacion de dos numero
def fMultiplicacion(num1,num2):
	"""
	Esta funcion calcula la multiplicacion de dos numeros en base a dos entradas de tipo float, num1 y num2, son de tipo float para poder realizar operaciones con decimales.
	"""
	#:param num1: Primero numero
	#:param num2: Segundo Numero
	#:type num1: float
	#:type num2: float
	#:return: Retorna la multiplicacion de num1*num2
	#:rtype: float
	

	print("\n\t-> El resultado de " + str(num1) + " * " + str(num2) + "= ", num1*num2)
	print("\t   Presione cualquier tecla para reiniciar el programa ...")
	input()
	return num1*num2
	
	

def fDivision(num1,num2):
	"""
	Esta funcion calcula la division de dos numeros  en base a dos entradas de tipo float, num1 y num2, son de tipo float para poder realizar operaciones con decimales.
	"""
	#:param num1: Dividendo
	#:param num2: Divisor
	#:type num1: float
	#:type num2: float
	#:return: Retorna la division de num1/num2
	#:rtype: float
	

	if num2 != 0:
		respuesta = num1 / num2
	
		
		print("\n\t-> El resultado de " + str(num1) + " / " + str(num2) + "= ", num1/num2)
	else:
		#Division entre cero no existe
		print("\n\t>>> Error. Division entre cero.")
		
	print("\t   Presione cualquier tecla para reiniciar el programa ...")
	input()
	return num1/num2
	
	

def fSalida():
	"""
	Esta funcion muestra un mensaje de despedida y cierra el programa
	"""
	submenu("Salida") 
	
	print("\n\t-> Que tenga buen dia. Hasta Luego.  ")
	print("\t   Presione cualquier tecla para cerrar el programa ...")
	input()
	




#de aqui hacia abajo esta el programa principal

resultado = 0
op= -1

while op != 0:
	op = opcion(0, 4, 0) #llama a la funcion opcion
	
	if op == 1:
		os.system('cls')
		#funcion Suma
		submenu("SUMA")
		num1 = float(input("\tIngrese un numero: "))
		num2 = float(input("\tIngrese un numero: "))
		resultado = fSuma(num1,num2)
		
	elif op == 2:
		os.system('cls')
		submenu("RESTA")
		num1 = float(input("\tIngrese un numero: "))
		num2 = float(input("\tIngrese un numero: "))
		#funcion Resta
		resultado = fResta(num1,num2)
		
	elif op == 3:
		os.system('cls')
		submenu("MULTIPLICACION")
		num1 = float(input("\tIngrese un numero: "))
		num2 = float(input("\tIngrese un numero: "))
		#funcion Multiplicacion
		resultado = fMultiplicacion(num1,num2)
		
	elif op == 4:
		os.system('cls')
		submenu("division") 
		num1 = float(input("\tIngrese un dividendo: "))
		num2 = float(input("\tIngrese un divisor: "))
		#funcion Division
		resultado = fDivision(num1,num2)

	else:
		#funcion Salida
		resultado = fSalida()
		
